export interface AppCard {
	dataset: Partial<{
		name: string;
		url: string;
		downloadUrl: string;
		imageName: string;
		imageUrl: string;
	}>;
}

export class AppCard extends HTMLElement {
	constructor() {
		super();
		this.#validate();
		this.#appendElements();
	}

	#appendElements() {
		const img = document.createElement("img");
		const h3 = document.createElement("h3");
		const actionsDiv = document.createElement("div");
		const downloadUrlA = document.createElement("a");
		const urlA = document.createElement("a");

		img.className = "app-logo";
		img.src = this.dataset.imageUrl!;
		img.alt = this.dataset.imageName!;

		h3.className = "app-name";
		h3.textContent = this.dataset.name!;

		downloadUrlA.className = "hyperlink-button app-download-url";
		downloadUrlA.rel = "noopener noreferrer nofollow";
		downloadUrlA.textContent = "Download (Windows x64)";
		downloadUrlA.target = "_blank";
		downloadUrlA.href = this.dataset.downloadUrl!;

		urlA.className = "hyperlink-button app-url";
		urlA.rel = "noopener noreferrer nofollow";
		urlA.textContent = "View source code";
		urlA.target = "_blank";
		urlA.href = this.dataset.url!;

		actionsDiv.className = "app-actions display-flex";
		actionsDiv.append(downloadUrlA, urlA);

		super.append(img, h3, actionsDiv);
	}

	#validate() {
		if (typeof this.dataset.name !== "string")
			throw new TypeError("name must be a string");

		if (typeof this.dataset.downloadUrl !== "string")
			throw new TypeError("downloadUrl must be a string");

		if (typeof this.dataset.url !== "string")
			throw new TypeError("url must be a string");

		if (typeof this.dataset.imageName !== "string")
			throw new TypeError("imageName must be a string");

		if (typeof this.dataset.imageUrl !== "string")
			throw new TypeError("imageUrl must be a string");
	}
}

window.customElements.define("x-app-card", AppCard);