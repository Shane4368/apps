import { copyFileSync } from "node:fs";
import { buildSync } from "esbuild";

buildSync({
	entryPoints: ["source/index.ts"],
	outfile: "static/index.js",
	format: "esm",
	target: "es2022",
	bundle: true,
	minify: true
});

copyFileSync("./source/index.html", "./static/index.html");